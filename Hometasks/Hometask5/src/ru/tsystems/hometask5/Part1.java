package ru.tsystems.hometask5;

/**
 * Created by apavlova on 13.11.2017.
 */
public class Part1 {
    //1
    public class Click{}
    public class Clack extends Click{}

    //2
    abstract class Top{}
    public class Tip extends Top{}

    //3
    abstract class Fi{}
    abstract class Fee extends Fi{}

    //4
    public interface Foo{}
    public class Bar implements Foo{}
    public class Baz extends Bar{}

    //5
    public interface Zeta{}
    public interface Beta{}
    public class Alpha implements Zeta{}
    public class Delta extends Alpha implements Beta{}


}
