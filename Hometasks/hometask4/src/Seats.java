/**
 * Created by apavlova on 02.11.2017.
 */
public class Seats {
    int row;
    Place place;
    Passanger passanger;

    public int getRow() {
        return row;
    }



    public Place getPlace() {
        return place;
    }

    public void setSeat(Integer row, Place place) {

        if (this.row > 5) {
            throw new IllegalStateException("Wrong row number");
            }
        if (this.place != place) {
            throw new IllegalStateException("Wrong place");
            }
        this.row = row;
        this.place = place;
    }

    public void setPassanger(Passanger passanger) {
        this.passanger = passanger;
    }
}
