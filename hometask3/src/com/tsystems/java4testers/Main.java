package com.tsystems.java4testers;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class Main {

    public static void main(String[] args) {
        String password = "somepwDDh1";
        if (isPasswordCorrect(password)) {
            System.out.println("Password correct");
        } else System.out.println("Password NOT CORRECT!!!");

    }

    private static boolean isPasswordCorrect(String password) {

        Pattern pattern = Pattern.compile("((?=.*\\d)(?=.*[a-z])(?=.*[A-Z]).{8,})");
        Matcher matcher = pattern.matcher(password);
        return matcher.matches();
    }
}

