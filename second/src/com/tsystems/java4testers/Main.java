package com.tsystems.java4testers;

import java.lang.reflect.Array;
import java.util.Arrays;
import java.util.Random;

public class Main {

    public static void main(String[] args) {
        byte[][] a = createArray();
        printArray(a);
    }

    private static byte[][] createArray() {
        byte[][] a = new byte[5][5];
        byte max = 10;
        int index = 0;

        for (int i = 0; i < a.length; i++) {
            for (int j = 0; j < a[i].length; j++) {

                boolean genNext;
                do {
                    byte nextRandom = (byte) (Math.random() * max);
                    byte[] rowsValues = new byte[5]; //a.length
                    byte[] colsValues = new byte[5]; //a[i].length

                    for (index = 0; index < 5; index++) {
                        rowsValues[index] = a[i][index];
                        }
                    for (index = 0; index < 5; index++) {
                            colsValues[index] = a[index][j];
                        }
                    genNext = false;
                    for (index = 0; index < 5; index++) {
                        if (rowsValues[index] == nextRandom || colsValues[index] == nextRandom) {
                            genNext = true;
                            break;
                        }
                    }

                    if (!genNext) {
                        a[i][j] = nextRandom;
                    }
                } while (genNext);
            }
        }

        return a;
    }

    private static void printArray(byte[][] a) {
        for (int i = 0; i < a.length; i++) {
            System.out.println();
            for (int j = 0; j < a[i].length; j++) {
                System.out.print(a[i][j] + " ");
            }
        }
    }
}




